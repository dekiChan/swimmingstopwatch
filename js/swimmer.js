/* ******************** ZVEZDINA STOPARCA ******************** //
// ----------------------------------------------------------- //
// ***** CLASS: Swimmer			 						 ***** //
// ----------------------------------------------------------- */

Swimmer = function(name, stopwatch_obj) {
	this.Name = name;

	this.Length = null;
	this.Current_length = 1;
	this.Is_swimming = false;

	this.Start_time = null;
	this.Intermediate_times = [];
	this.End_time = null;

	this.Stopwatch = stopwatch_obj;
}

Swimmer.prototype.isSwimming = function() {
	return this.Is_swimming;
}
Swimmer.prototype.toggleSwimming = function() {
	this.Is_swimming = !this.Is_swimming;
}

// ** get/set/reset data methods
Swimmer.prototype.setName = function(name) {
	this.Name = name;
};

Swimmer.prototype.setLength = function(length) {
	this.Length = length;
};
Swimmer.prototype.incrementLength = function() {
	this.Current_length++;
}
Swimmer.prototype.resetFinalLength = function() {
	this.Length = null;
};
Swimmer.prototype.resetCurrentLength = function() {
	this.Current_length = 1;
};
Swimmer.prototype.getCurrentLength = function() {
	return this.Current_length;
}
Swimmer.prototype.getFinalLength = function() {
	return this.Length;
}

Swimmer.prototype.setStartTime = function(time) {
	this.Start_time = time;
};

Swimmer.prototype.setEndTime = function(time) {
	this.End_time = time;
};

Swimmer.prototype.setIntermediateTime = function(time) {
	var intermediate_time = 0;

	if (this.Intermediate_times.length == 0) {
		intermediate_time = time - this.Start_time;
	} else {
		intermediate_time = time - this.Intermediate_times[-1];
	}

	this.Intermediate_times.push(time);
};

Swimmer.prototype.resetTime = function() {
	this.Start_time = null;
	this.End_time = null;
	this.Intermediate_times = null;
};

Swimmer.prototype.resetAll = function() {
	this.resetTime();
	this.resetCurrentLength();
	this.resetFinalLength();
	this.Stopwatch.stop();
	this.Stopwatch.reset();
}

// ** Stopwatch related functions
// Swimmer.prototype.