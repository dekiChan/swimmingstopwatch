/* ******************** ZVEZDINA STOPARCA ******************** //
// ----------------------------------------------------------- //
// ***** SERVER					 						 ***** //
// ----------------------------------------------------------- */

// load all necessary node modules
var http = require('http');
var url = require('url');
var fs = require('fs');

// create server
var server = http.createServer(function(req, res) {
	onRequest(req, res);
});

// listen for requests on port 3001
server.listen(8888, 'localhost');

// functions
var onRequest = function(req, res) {
	console.log('wi bi in onRequest');
	var url_data = url.parse(req.url, true);
	// debugging
	console.log('New request: ' + url_data.pathname);

	// routing
	serveRequest(url_data);
}

var serveRequest = function(url_data) {
	console.log('wi bi in serveRequest');
	console.log(url_data);
}