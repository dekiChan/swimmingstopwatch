/* ******************** ZVEZDINA STOPARCA ******************** //
// ----------------------------------------------------------- //
// ***** CLASS: Checker			 						 ***** //
// ** Functions that check/validate data, such as whether	** //
// ** input is integer or is float 	                      	** //
// ----------------------------------------------------------- */

Checker = function() {}

Checker.prototype.isInteger = function(f) {
    return typeof(f)==="number" && Math.round(f) == f;
}

Checker.prototype.isFloat = function(f) {
	return typeof(f)==="number" && !isInteger(f);
}