/* ******************** ZVEZDINA STOPARCA ******************** //
// ----------------------------------------------------------- //
// ***** MAIN SCRIPT			 						 ***** //
// ----------------------------------------------------------- */
	
// ------------------------------------------------------- //
// **** define constants and helper objects/functions **** //
// ------------------------------------------------------- //
var _checker = new Checker();

var swimmer_names = [
					'Vuk',
					'Jernej',
					'Špela',
					'Nastja',
					'Marcel',
					'Ana',
					'Žana',
					'Anja'
					];

var swimmer_charcodes = {
	'v': 'vuk',
	'j': 'jernej',
	'š': 'špela',
	'n': 'nastja',
	'm': 'marcel',
	'a': 'ana',
	'ž': 'žana',
	'q': 'anja'
};

var swimmers = {};

var swimmer_container_template = 
				'<div class="col-1-4" class="temp_container_id" id="">' +
					'<h2 class="title-mid temp_name"></h2>' +
					'<div class="length-settings">' +
						'<input type="number" class="length-input temp_input_id" id="" placeholder="razdalja [m]">' +
						'<input type="button" class="length-confirm temp_button_ok_id" id="" value="OK">' +
					'</div>' +
					'<div class="time-box temp_intermediate_time_b">' +
						'<h3 class="title-min">Časi:</h3>' +
						'<p class="intermediate_time temp_intermediate_time" id="">100m - -:--.--</p>' +
					'</div>' +
					'<div class="time-box">' +
						'<h3 class="title-min">Trenutni čas:</h3>' +
						'<p class="temp_c_time_id" id="">-:--.--</p>' +
					'</div>' +
					'<div class="time-box">' +
						'<h3 class="title-min">Končni čas:</h3>' +
						'<p class="temp_e_time_id" id="">-:--.--</p>' +
						'<input type="button" class="save-end-time temp_save_time1" id="" value="Shrani rezultate">' +
						'<input type="button" class="reset-display-button temp_save_time2" id="" value="Resetiraj">' +
					'</div>' +
				'</div>';

// ------------------------------ //
// **** function definitions **** //
// ------------------------------ //
var swimmerIdFromChar = function(code) {
	return swimmer_charcodes[code];
}

var createSwimmers = function() {
	var name = null;
	var stopwatch = null;

	for (var i = swimmer_names.length - 1; i >= 0; i--) {
		name = swimmer_names[i];
		stopwatch = new Stopwatch(timeDisplay, 10, name.toLowerCase()); // name.toLowerCase serves as swimmer_id

		swimmers[name.toLowerCase()] = new Swimmer(name, stopwatch);
	};
}

var initPage = function() {

	for (var i = 0; i < swimmer_names.length; i++) {
		var name_lc = swimmer_names[i].toLowerCase();

		// create new row for every 4 swimmers
		if ((i == 0) || (i % 4 == 0)) {
			$('.content').append('<div class="col-1-1">');
		}

		// add new swimmer container
		$('.content').append(swimmer_container_template);
		var container = $('.temp_container_id'); // see line 18
		container.removeClass('temp_container_id');

		// populate container template with correct names and ids
		var name_title = $('.temp_name');
		name_title.text(swimmer_names[i]);
		name_title.removeClass('temp_name');

		var length_input = $('.temp_input_id');
		length_input.attr('id', name_lc + '-length');
		length_input.removeClass('temp_input_id');

		var ok_button = $('.temp_button_ok_id');
		ok_button.attr('id', name_lc + '-ok-button');
		ok_button.removeClass('temp_button_ok_id');

		var int_time_b = $('.temp_intermediate_time_b');
		int_time_b.attr('id', name_lc + '-int-time-display');
		int_time_b.removeClass('temp_intermediate_time_b');

		var int_time = $('.temp_intermediate_time');
		int_time.attr('id', name_lc + '-int-time-1');
		int_time.removeClass('temp_intermediate_time');

		var c_time = $('.temp_c_time_id');
		c_time.attr('id', name_lc + '-c-time');
		c_time.removeClass('temp_c_time_id');

		var e_time = $('.temp_e_time_id');
		e_time.attr('id', name_lc + '-e-time');
		e_time.removeClass('temp_e_time_id');

		var s_time = $('.temp_save_time1');
		s_time.attr('id', name_lc + '-s-time');
		s_time.removeClass('temp_save_time1');

		var s_time = $('.temp_save_time2');
		s_time.attr('id', name_lc + '-r-display');
		s_time.removeClass('temp_save_time2');

		// close the row (end of <div class="col-1-1">), see lines 66-69
		if ((i == 0) || (i % 4 == 0)) {
			$('.content').append('</div>');
		}
	};
}

var lengthHandler = function(swimmer_id, no_of_lengths) {
	var container = $('#' + swimmer_id + '-int-time-display');
	resetIntermediateTimeDisplay(container, swimmer_id);
	resetFinalTimeDisplay(swimmers[swimmer_id]);
	resetTimeDisplay(swimmers[swimmer_id].Stopwatch);

	for (var i = 2; i < no_of_lengths+1; i++) {
		var int_time_p = '<p class="intermediate_time" id="' + swimmer_id + '-int-time-' + i +'">' +
						(100 * i) + 'm - -:--.--</p>';
		container.append(int_time_p);
	};
}

var timeDisplay = function(stopwatch_obj) {
	var elapsed = stopwatch_obj.getElapsed();
	var display_id = stopwatch_obj.getDisplayId() + '-c-time';

	$('p#' + display_id).text(elapsed.minutes + ':' + elapsed.seconds + '.' + elapsed.milliseconds);
}

var resetTimeDisplay = function(stopwatch_obj) {
	var display_id = stopwatch_obj.getDisplayId() + '-c-time';

	$('p#' + display_id).text('--:--.--');
}

var updateIntermediateTimeDisplay = function(swimmer_obj) {
	var current_length = swimmer_obj.getCurrentLength();
	var elapsed = swimmer_obj.Stopwatch.getElapsed();
	var display_id = swimmer_obj.Stopwatch.getDisplayId() + '-int-time-' + current_length;
	var intermediate_display = $('#' + display_id);
	var display_template = (current_length * 100) + 'm - ' + elapsed.minutes + ':' + elapsed.seconds + '.' + elapsed.milliseconds;

	intermediate_display.text(display_template);
}

var updateFinalTimeDisplay = function(swimmer_obj) {
	var display_id = swimmer_obj.Stopwatch.getDisplayId() + '-e-time';
	var elapsed = swimmer_obj.Stopwatch.getElapsed();
	var end_display = $('#' + display_id);

	end_display.text(elapsed.minutes + ':' + elapsed.seconds + '.' + elapsed.milliseconds);
}

var resetFinalTimeDisplay = function(swimmer_obj) {
	var display_id = swimmer_obj.Stopwatch.getDisplayId() + '-e-time';
	var end_display = $('#' + display_id);

	end_display.text('-:--.--');
}

var resetIntermediateTimeDisplay = function(container, swimmer_id) {
	var title = container.children('h3');

	container.empty();

	container.append(title);
	container.append('<p class="intermediate_time temp_intermediate_time" id="">100m - -:--.--</p>');
	$('.temp_intermediate_time').attr('id', swimmer_id + '-int-time-1').removeClass('temp_intermediate_time');

}

var resetSwimmerDisplay = function(swimmer_id) {
	$('#' + swimmer_id + '-length').val('');
	resetIntermediateTimeDisplay($('#' + swimmer_id + '-int-time-display'), swimmer_id);
	resetTimeDisplay(swimmers[swimmer_id].Stopwatch);
	resetFinalTimeDisplay(swimmers[swimmer_id]);

	swimmers[swimmer_id].resetAll();
}

var resetAllSwimmersDisplay = function() {
	for (var i = 0; i < swimmer_names.length; i++) {
		resetSwimmerDisplay(swimmer_names[i].toLowerCase());
	};
}

var setSwimmerLength = function(swimmer_id, no_of_lengths) {
	swimmer_id = swimmer_id.split('-');
	swimmer_id = swimmer_id[0];

	if (no_of_lengths == null) {
		var no_of_lengths = $('#' + swimmer_id + '-length').val();
		no_of_lengths /= 100;
		// check if no_of_lengths is integer
		if (!_checker.isInteger(no_of_lengths)) {
			alert('Dolžina ni večkratnik 100.');
			return;
		}
	}

	swimmers[swimmer_id].setLength(no_of_lengths);

	lengthHandler(swimmer_id, no_of_lengths);
}

var timeMeasuring = function(swimmer_id) {
	var swimmer = swimmers[swimmer_id];

	// check f length is set up
	if (swimmer.getFinalLength() === null) {
		alert('Dolžina plavanja ni nastavljena.');
		return;
	}

	// swimmer finished
	if (swimmer.isSwimming() && (swimmer.getCurrentLength() == swimmer.getFinalLength())) {
		swimmer.Stopwatch.stop();
		updateIntermediateTimeDisplay(swimmer);
		resetTimeDisplay(swimmer.Stopwatch);
		updateFinalTimeDisplay(swimmer);
		swimmer.toggleSwimming(); // .isSwimming() now returns false
		swimmer.resetCurrentLength();
		swimmer.Stopwatch.reset();

	// swimmer has not yet completed all lengths
	} else if (swimmer.isSwimming()) {
		updateIntermediateTimeDisplay(swimmer);
		swimmer.incrementLength();

	// swimmer started
	} else {
		lengthHandler(swimmer_id, swimmer.getFinalLength());
		swimmer.toggleSwimming(); // .isSwimming() now returns true
		swimmer.Stopwatch.start();
	}
}


$(document).ready(function() {

	// ------------------------- //
	// **** initialize page **** //
	// ------------------------- //
	createSwimmers();
	initPage();

	// ------------------------------ //
	// **** capture click events **** //
	// ------------------------------ //

	// set general length
	$('#general-length-confirm-button').on('click', function() {
		var no_of_lengths = $('#general-length-input').val()/100;

		// check if no_of_lengths is integer
		if (!_checker.isInteger(no_of_lengths)) {
			alert('Dolžina ni večkratnik 100.');
			return;
		}

		for (var i = 0; i < swimmer_names.length; i++) {
			setSwimmerLength(swimmer_names[i].toLowerCase(), no_of_lengths);		
		};
	});

	// start all at the same time
	$('#start-all').on('click', function() {
		for (var i = 0; i < swimmer_names.length; i++) {
			timeMeasuring(swimmer_names[i].toLowerCase());			
		};
	});

	// reset swimmers display
	$('.reset-display-button').on('click', function() {
		var swimmer_id = $(this).attr('id');
		swimmer_id = swimmer_id.split('-');
		swimmer_id = swimmer_id[0];

		resetSwimmerDisplay(swimmer_id);
	});

	$('#reset-all').on('click', function() {
		resetAllSwimmersDisplay();
	});

	// refresh intermediate times display
	$('.length-confirm').on('click',function() {
		var swimmer_id = $(this).attr('id');

		setSwimmerLength(swimmer_id, null);
	});

	// --------------------------------- //
	// **** capture keypress events **** //
	// --------------------------------- //
	$(document).keypress(function(event) {
		var key = String.fromCharCode(event.which);

		if (!isNaN(key)) {
			// if key is number then let it go :)
			return;
		}

		var swimmer_id = swimmerIdFromChar(key);

		timeMeasuring(swimmer_id);
	});
});